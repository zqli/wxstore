'use strict';

/**
 * 商城
 * StoreController
 * @constructor
 */
WxStoreApp.controller('StoreController', ["$rootScope","$scope","$http",
  function($rootScope, $scope, $http) {
	$scope.fetchGoodsList = function() {
        $http.get('Store/goodslist.json').success(function(goodsList){
            $scope.goodsList = goodsList;
            console.log("goodsList:"+goodsList);
        }).error(function(data, status, headers, config) {
        	$rootScope.wxstore.hideLoading();
        	console.log(data+"-"+status+"-"+headers+"-"+config);
        	$rootScope.wxstore.showError(data);
        });
    };
    
    $scope.fetchGoodsList();
	
}]);


