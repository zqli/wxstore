package com.ly.wxstore.entity.goods;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class GoodsImages {

	public GoodsImages() {
	}
	
	private Long id; //
	private Long goodsId; //
	private String imageUrl; //一级销售提成
	private Date createDate; //创建时间
	private Date updateDate; //最后更新时间
	private String createUserUid; //创建人uid
	private String updateUserUid; //最后更新人uid
	private Integer deleted; //是否删除（0：删，1：用）
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *
     **/
	public Long getGoodsId(){
		return goodsId;
	}
	
	/**
	 *
	 **/
	public void setGoodsId(Long goodsId){
		this.goodsId=goodsId;
	}
    /**
     *一级销售提成
     **/
	public String getImageUrl(){
		return imageUrl;
	}
	
	/**
	 *一级销售提成
	 **/
	public void setImageUrl(String imageUrl){
		this.imageUrl=imageUrl;
	}
    /**
     *创建时间
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *创建时间
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *最后更新时间
     **/
	public Date getUpdateDate(){
		return updateDate;
	}
	
	/**
	 *最后更新时间
	 **/
	public void setUpdateDate(Date updateDate){
		this.updateDate=updateDate;
	}
    /**
     *创建人uid
     **/
	public String getCreateUserUid(){
		return createUserUid;
	}
	
	/**
	 *创建人uid
	 **/
	public void setCreateUserUid(String createUserUid){
		this.createUserUid=createUserUid;
	}
    /**
     *最后更新人uid
     **/
	public String getUpdateUserUid(){
		return updateUserUid;
	}
	
	/**
	 *最后更新人uid
	 **/
	public void setUpdateUserUid(String updateUserUid){
		this.updateUserUid=updateUserUid;
	}
    /**
     *是否删除（0：删，1：用）
     **/
	public Integer getDeleted(){
		return deleted;
	}
	
	/**
	 *是否删除（0：删，1：用）
	 **/
	public void setDeleted(Integer deleted){
		this.deleted=deleted;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}