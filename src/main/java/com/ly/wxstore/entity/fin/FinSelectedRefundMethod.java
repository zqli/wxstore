package com.ly.wxstore.entity.fin;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class FinSelectedRefundMethod {

	public FinSelectedRefundMethod() {
	}
	
	private Long id; //
	private Long withdrawalApplicationId; //提款申请id
	private String applySid; //收款人账号
	private Date applyDate; //收款人姓名
	private Long paymentMethodId; //收款方式id
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
    /**
     *提款申请id
     **/
	public Long getWithdrawalApplicationId(){
		return withdrawalApplicationId;
	}
	
	/**
	 *提款申请id
	 **/
	public void setWithdrawalApplicationId(Long withdrawalApplicationId){
		this.withdrawalApplicationId=withdrawalApplicationId;
	}
    /**
     *收款人账号
     **/
	public String getApplySid(){
		return applySid;
	}
	
	/**
	 *收款人账号
	 **/
	public void setApplySid(String applySid){
		this.applySid=applySid;
	}
    /**
     *收款人姓名
     **/
	public Date getApplyDate(){
		return applyDate;
	}
	
	/**
	 *收款人姓名
	 **/
	public void setApplyDate(Date applyDate){
		this.applyDate=applyDate;
	}
    /**
     *收款方式id
     **/
	public Long getPaymentMethodId(){
		return paymentMethodId;
	}
	
	/**
	 *收款方式id
	 **/
	public void setPaymentMethodId(Long paymentMethodId){
		this.paymentMethodId=paymentMethodId;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}