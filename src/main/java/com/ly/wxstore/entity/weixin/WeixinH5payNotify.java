package com.ly.wxstore.entity.weixin;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

@XmlRootElement(name = "xml")
public class WeixinH5payNotify {

	public WeixinH5payNotify() {
	}
	
	private Long id; //
	
	
	private String returnCode; //UCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	
	
	private String returnMsg; //返回信息，如非空，为错误原因：签名失败、参数格式校验错误
	
	
	private String appid; //
	
	
	private String mchId; //微信支付分配的商户号
	
	
	private String deviceInfo; //微信支付分配的终端设备号
	
	
	private String nonceStr; //随机字符串，不长于32位
	
	
	private String sign; //
	
	
	private String resultCode; //业务结果:SUCCESS/FAIL
	
	
	private String errCode; //错误代码:SYSTEMERROR
	
	
	private String errCodeDes; //错误代码描述:系统错误
	
	
	private String openid; //
	
	
	private String isSubscribe; //用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
	
	
	private String tradeType; //交易类型:JSAPI、NATIVE、APP
	
	
	private String bankType; //付款银行:银行类型，采用字符串类型的银行标识，银行类型见银行列表
	
	
	private Long totalFee; //订单总金额，单位为分

	
	private String feeType; //
	
	
	private Long cashFee; //

	
	private String cashFeeType; //货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	
	
	private Long couponFee; //代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额
	
	
	private Long couponCount; //代金券或立减优惠使用数量
	
	
	private Long couponId0; //代金券或立减优惠ID,$n为下标，从0开始编号
	
	
	private Long couponFee0; //单个代金券或立减优惠支付金额,$n为下标，从0开始编号
	
	
	private String transactionId; //微信支付订单号
	
	
	private String outTradeNo; //商户系统的订单号，与请求一致。

	
	private String attach; //商家数据包，原样返回
	
	
	private String timeEnd; //
	
	private Date createDate; //
	private String mySign; //如果数据接收后的签名与sign字段不一样，说明数据被篡改
    
    /**
     *
     **/
	public Long getId(){
		return id;
	}
	
	/**
	 *
	 **/
	public void setId(Long id){
		this.id=id;
	}
	
    /**
     *UCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
     **/
	@XmlElement(name="return_code")
	public String getReturnCode(){
		return returnCode;
	}
	
	/**
	 *UCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	 **/
	public void setReturnCode(String returnCode){
		this.returnCode=returnCode;
	}
    /**
     *返回信息，如非空，为错误原因：签名失败、参数格式校验错误
     **/
	@XmlElement(name="return_msg")
	public String getReturnMsg(){
		return returnMsg;
	}
	
	/**
	 *返回信息，如非空，为错误原因：签名失败、参数格式校验错误
	 **/
	public void setReturnMsg(String returnMsg){
		this.returnMsg=returnMsg;
	}
    /**
     *
     **/
	@XmlElement(name="appid")
	public String getAppid(){
		return appid;
	}
	
	/**
	 *
	 **/
	public void setAppid(String appid){
		this.appid=appid;
	}
    /**
     *微信支付分配的商户号
     **/
	@XmlElement(name="mch_id")
	public String getMchId(){
		return mchId;
	}
	
	/**
	 *微信支付分配的商户号
	 **/
	public void setMchId(String mchId){
		this.mchId=mchId;
	}
    /**
     *微信支付分配的终端设备号
     **/
	@XmlElement(name="device_info")
	public String getDeviceInfo(){
		return deviceInfo;
	}
	
	/**
	 *微信支付分配的终端设备号
	 **/
	public void setDeviceInfo(String deviceInfo){
		this.deviceInfo=deviceInfo;
	}
    /**
     *随机字符串，不长于32位
     **/
	@XmlElement(name="nonce_str")
	public String getNonceStr(){
		return nonceStr;
	}
	
	/**
	 *随机字符串，不长于32位
	 **/
	public void setNonceStr(String nonceStr){
		this.nonceStr=nonceStr;
	}
    /**
     *
     **/
	@XmlElement(name="sign")
	public String getSign(){
		return sign;
	}
	
	/**
	 *
	 **/
	public void setSign(String sign){
		this.sign=sign;
	}
    /**
     *业务结果:SUCCESS/FAIL
     **/
	@XmlElement(name="result_code")
	public String getResultCode(){
		return resultCode;
	}
	
	/**
	 *业务结果:SUCCESS/FAIL
	 **/
	public void setResultCode(String resultCode){
		this.resultCode=resultCode;
	}
    /**
     *错误代码:SYSTEMERROR
     **/
	@XmlElement(name="err_code")
	public String getErrCode(){
		return errCode;
	}
	
	/**
	 *错误代码:SYSTEMERROR
	 **/
	public void setErrCode(String errCode){
		this.errCode=errCode;
	}
    /**
     *错误代码描述:系统错误
     **/
	@XmlElement(name="err_code_des")
	public String getErrCodeDes(){
		return errCodeDes;
	}
	
	/**
	 *错误代码描述:系统错误
	 **/
	public void setErrCodeDes(String errCodeDes){
		this.errCodeDes=errCodeDes;
	}
    /**
     *
     **/
	@XmlElement(name="openid")
	public String getOpenid(){
		return openid;
	}
	
	/**
	 *
	 **/
	public void setOpenid(String openid){
		this.openid=openid;
	}
    /**
     *用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
     **/
	@XmlElement(name="is_subscribe")
	public String getIsSubscribe(){
		return isSubscribe;
	}
	
	/**
	 *用户是否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
	 **/
	public void setIsSubscribe(String isSubscribe){
		this.isSubscribe=isSubscribe;
	}
    /**
     *交易类型:JSAPI、NATIVE、APP
     **/
	@XmlElement(name="trade_type")
	public String getTradeType(){
		return tradeType;
	}
	
	/**
	 *交易类型:JSAPI、NATIVE、APP
	 **/
	public void setTradeType(String tradeType){
		this.tradeType=tradeType;
	}
    /**
     *付款银行:银行类型，采用字符串类型的银行标识，银行类型见银行列表
     **/
	@XmlElement(name="bank_type")
	public String getBankType(){
		return bankType;
	}
	
	/**
	 *付款银行:银行类型，采用字符串类型的银行标识，银行类型见银行列表
	 **/
	public void setBankType(String bankType){
		this.bankType=bankType;
	}
    /**
     *订单总金额，单位为分
     **/
	@XmlElement(name="total_fee")
	public Long getTotalFee(){
		return totalFee;
	}
	
	/**
	 *订单总金额，单位为分
	 **/
	public void setTotalFee(Long totalFee){
		this.totalFee=totalFee;
	}
    /**
     *
     **/
	@XmlElement(name="fee_type")
	public String getFeeType(){
		return feeType;
	}
	
	/**
	 *
	 **/
	public void setFeeType(String feeType){
		this.feeType=feeType;
	}
    /**
     *
     **/
	@XmlElement(name="cash_fee")
	public Long getCashFee(){
		return cashFee;
	}
	
	/**
	 *
	 **/
	public void setCashFee(Long cashFee){
		this.cashFee=cashFee;
	}
    /**
     *货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
     **/
	@XmlElement(name="cash_fee_type")
	public String getCashFeeType(){
		return cashFeeType;
	}
	
	/**
	 *货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	 **/
	public void setCashFeeType(String cashFeeType){
		this.cashFeeType=cashFeeType;
	}
    /**
     *代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额
     **/
	@XmlElement(name="coupon_fee")
	public Long getCouponFee(){
		return couponFee;
	}
	
	/**
	 *代金券或立减优惠金额<=订单总金额，订单总金额-代金券或立减优惠金额=现金支付金额
	 **/
	public void setCouponFee(Long couponFee){
		this.couponFee=couponFee;
	}
    /**
     *代金券或立减优惠使用数量
     **/
	@XmlElement(name="coupon_count")
	public Long getCouponCount(){
		return couponCount;
	}
	
	/**
	 *代金券或立减优惠使用数量
	 **/
	public void setCouponCount(Long couponCount){
		this.couponCount=couponCount;
	}
    /**
     *代金券或立减优惠ID,$n为下标，从0开始编号
     **/
	@XmlElement(name="coupon_id_0")
	public Long getCouponId0(){
		return couponId0;
	}
	
	/**
	 *代金券或立减优惠ID,$n为下标，从0开始编号
	 **/
	public void setCouponId0(Long couponId0){
		this.couponId0=couponId0;
	}
    /**
     *单个代金券或立减优惠支付金额,$n为下标，从0开始编号
     **/
	@XmlElement(name="coupon_fee_0")
	public Long getCouponFee0(){
		return couponFee0;
	}
	
	/**
	 *单个代金券或立减优惠支付金额,$n为下标，从0开始编号
	 **/
	public void setCouponFee0(Long couponFee0){
		this.couponFee0=couponFee0;
	}
    /**
     *微信支付订单号
     **/
	@XmlElement(name="transaction_id")
	public String getTransactionId(){
		return transactionId;
	}
	
	/**
	 *微信支付订单号
	 **/
	public void setTransactionId(String transactionId){
		this.transactionId=transactionId;
	}
    /**
     *商户系统的订单号，与请求一致。
     **/
	@XmlElement(name="out_trade_no")
	public String getOutTradeNo(){
		return outTradeNo;
	}
	
	/**
	 *商户系统的订单号，与请求一致。
	 **/
	public void setOutTradeNo(String outTradeNo){
		this.outTradeNo=outTradeNo;
	}
    /**
     *商家数据包，原样返回
     **/
	@XmlElement(name="attach")
	public String getAttach(){
		return attach;
	}
	
	/**
	 *商家数据包，原样返回
	 **/
	public void setAttach(String attach){
		this.attach=attach;
	}
    /**
     *
     **/
	@XmlElement(name="time_end")
	public String getTimeEnd(){
		return timeEnd;
	}
	
	/**
	 *
	 **/
	public void setTimeEnd(String timeEnd){
		this.timeEnd=timeEnd;
	}
    /**
     *
     **/
	public Date getCreateDate(){
		return createDate;
	}
	
	/**
	 *
	 **/
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
    /**
     *如果数据接收后的签名与sign字段不一样，说明数据被篡改
     **/
	public String getMySign(){
		return mySign;
	}
	
	/**
	 *如果数据接收后的签名与sign字段不一样，说明数据被篡改
	 **/
	public void setMySign(String mySign){
		this.mySign=mySign;
	}
   
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}