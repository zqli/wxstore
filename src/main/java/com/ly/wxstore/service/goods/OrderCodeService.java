package com.ly.wxstore.service.goods;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.DateUtil;
import com.ly.wxstore.service.weixin.WeixinConf;

/**
 * 订单编号service
 * 
 * @author Peter
 *
 */
// Spring Service Bean的标识.
@Component
public class OrderCodeService {

	private static DecimalFormat df = new DecimalFormat("00000000.##");

	@Autowired
	private WeixinConf weixinConf;

	/**
	 * 
	 * @return 订单编号<br/><br/>
	 * 格式：weixinID + "-" + "yyyyMMddhhmmssSSS" + "五位随机数"<br/><br/>
	 * 例如：120150601220315567000860
	 */
	public String getOrderCode() {
		Random r = new Random();

		String orderCode =  weixinConf.getPublicId()+"" + DateUtil.sdf_4.format(new Date()) + "-" + df.format(r.nextInt(100000000));

		return orderCode;
	}
	

}
