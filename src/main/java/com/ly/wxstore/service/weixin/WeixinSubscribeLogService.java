package com.ly.wxstore.service.weixin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.MyPage;
import com.ly.wxstore.entity.weixin.WeixinSubscribeLog;
import com.ly.wxstore.repository.weixin.WeixinSubscribeLogDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class WeixinSubscribeLogService {

	@Autowired
	private WeixinSubscribeLogDao weixinSubscribeLogDao;

	public WeixinSubscribeLog getById(Long id) {
		return weixinSubscribeLogDao.getById(id);
	}

	public List<WeixinSubscribeLog> getAll() {
		return weixinSubscribeLogDao.getAll();
	}

	/**
	 * 分页查询
	 * 
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	public MyPage<WeixinSubscribeLog> searchPage(WeixinSubscribeLog weixinSubscribeLog, int currentPage, int pageSize) {
		MyPage<WeixinSubscribeLog> myPage = new MyPage<WeixinSubscribeLog>();

		Long count = weixinSubscribeLogDao.searchCount(weixinSubscribeLog);

		int pageStart = (currentPage - 1) < 0 ? 0 : (currentPage - 1) * pageSize;
		List<WeixinSubscribeLog> list = weixinSubscribeLogDao.searchPage(weixinSubscribeLog, pageStart, pageSize);

		myPage.setCount(count);
		myPage.setContent(list);

		return myPage;
	}

	public void save(WeixinSubscribeLog weixinSubscribeLog) {
		weixinSubscribeLogDao.save(weixinSubscribeLog);
	}

	public void update(WeixinSubscribeLog weixinSubscribeLog) {
		weixinSubscribeLogDao.update(weixinSubscribeLog);
	}

	/**
	 * 软删除
	 */
	public void delete(Long id) {
		weixinSubscribeLogDao.delete(id);
	}
}
