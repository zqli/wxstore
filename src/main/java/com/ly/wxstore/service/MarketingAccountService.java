package com.ly.wxstore.service;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ly.wxstore.comm.SalerRole;
import com.ly.wxstore.entity.MarketingAccount;
import com.ly.wxstore.repository.MarketingAccountDao;

/**
 * 
 * 
 * @author Peter
 */
// Spring Service Bean的标识.
@Component
public class MarketingAccountService {

	private static Logger logger = LoggerFactory.getLogger(MarketingAccountService.class);

	// 微信ID
	@Value("${WeiXin.PublicId}")
	private Long publicId;
	
	@Autowired
	private MarketingAccountDao marketingAccountDao;
	
	@Autowired
	private DictUmcodeSeedService umcodeSeedService;

	/**
	 *
	 *创建微营销用户账号,用户普通关注微信号时的注册方法
	 *
	 * @param openid
	 * 
	 */
	public synchronized MarketingAccount register(String openid, String registerFrom) {
		logger.info("register");
		MarketingAccount marketingAccount = marketingAccountDao.getByOpenid(openid);
		
		//未注册过账号
		if(marketingAccount==null){
			String sid= UUID.randomUUID().toString();
			
			marketingAccount = new MarketingAccount();
			marketingAccount.setPid(0L);
			marketingAccount.setCreateDate(new Date());
			marketingAccount.setOpenid(openid);//微信OpenID
			marketingAccount.setSid(sid);//二维码场景值sid
			marketingAccount.setParentSid(null);//父级二维码场景值parent_sid
			marketingAccount.setWeixinPublicId(publicId);
			marketingAccount.setUmcode(umcodeSeedService.getUmcode(publicId));//用户营销编号
			marketingAccount.setTopSid(sid);//合伙人Sid(系统预留字段：用于支持合伙人的销售模式)
			marketingAccount.setBoss(0L);//是否拥有微信公众号（boss）
			marketingAccount.setDepth(1L);//树的深度
			marketingAccount.setSalerRole(SalerRole.fans);//销售级别：粉丝
			marketingAccount.setRegisterFrom(registerFrom);//通过扫码关注 注册账号
			
			marketingAccountDao.save(marketingAccount);

		}
		
		return marketingAccount;
	}

	/**
	 * 通过扫码关注 注册账号<br/>
	 * 创建微营销用户账号，用户扫码关注微信公众号时 的注册方法
	 * @param openid
	 * @param parentSceneStr
	 */
	public synchronized void register(String openid, String parentSid,String registerFrom) {
		MarketingAccount marketingAccount = marketingAccountDao.getByOpenid(openid);
		
		//未注册过账号
		if(marketingAccount==null){
			MarketingAccount parent = marketingAccountDao.getBySid(parentSid);
			
			marketingAccount = new MarketingAccount();
			marketingAccount.setPid(parent.getId());
			marketingAccount.setCreateDate(new Date());
			marketingAccount.setOpenid(openid);//微信OpenID
			marketingAccount.setSid(UUID.randomUUID().toString());//二维码场景值sid
			marketingAccount.setParentSid(parentSid);//父级二维码场景值parent_sid
			marketingAccount.setWeixinPublicId(publicId);
			marketingAccount.setUmcode(umcodeSeedService.getUmcode(publicId));//用户营销编号
			marketingAccount.setTopSid(parent.getTopSid());//合伙人Sid(系统预留字段：用于支持合伙人的销售模式)
			marketingAccount.setBoss(0L);//是否拥有微信公众号（boss）
			marketingAccount.setDepth(parent.getDepth()+1);//树的深度
			marketingAccount.setSalerRole(SalerRole.fans);//销售级别：粉丝
			marketingAccount.setRegisterFrom(registerFrom);//通过扫码关注 注册账号
			
			marketingAccountDao.save(marketingAccount);

		}
	}

	public MarketingAccount getByOpenid(String openid) {
		return marketingAccountDao.getByOpenid(openid);
	}

	
}
