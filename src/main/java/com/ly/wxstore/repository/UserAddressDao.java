package com.ly.wxstore.repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.UserAddress;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface UserAddressDao {
	
	UserAddress getById(Long id);
	
	List<UserAddress> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<UserAddress> searchPage(@Param("userAddress")UserAddress userAddress,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(UserAddress userAddress);
	
	void save(UserAddress userAddress);
	
	void update(UserAddress userAddress);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	List<UserAddress> getDefaultBySid(String sid);

	List<UserAddress> getAllBySid(String sid);
	

}
