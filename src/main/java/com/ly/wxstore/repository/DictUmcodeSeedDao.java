package com.ly.wxstore.repository;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.DictUmcodeSeed;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface DictUmcodeSeedDao {
	
	DictUmcodeSeed getByWeixinPublicId(Long weixinPublicId);

	void save(DictUmcodeSeed umcodeSeed);

	void updateUmcodeSeed(@Param("id")Long id, @Param("umcodeSeed")String umcodeSeed);

}
