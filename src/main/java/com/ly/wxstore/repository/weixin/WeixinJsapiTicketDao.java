package com.ly.wxstore.repository.weixin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ly.wxstore.entity.weixin.WeixinJsapiTicket;
import com.ly.wxstore.repository.MyBatisRepository;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author peter
 */
@MyBatisRepository
public interface WeixinJsapiTicketDao {
	
	WeixinJsapiTicket getById(Long id);
	
	List<WeixinJsapiTicket> getAll();
	
	/**
	 * 分页查询
	 * @param overtime
	 * @param pageStart
	 * @param pageSize
	 * @return
	 */
	List<WeixinJsapiTicket> searchPage(@Param("weixinJsapiTicket")WeixinJsapiTicket weixinJsapiTicket,@Param("pageStart")int pageStart,@Param("pageSize")int pageSize);
	
	/**
	 * 分页查询总记录数
	 * @param overtime
	 * @return
	 */
	Long searchCount(WeixinJsapiTicket weixinJsapiTicket);
	
	void save(WeixinJsapiTicket weixinJsapiTicket);
	
	void update(WeixinJsapiTicket weixinJsapiTicket);
	
	/**
	 * 软删除
	 */
	void delete(Long id);

	WeixinJsapiTicket getByWeixinId(Long weixinPublicId);
	

}
