package com.ly.wxstore.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.exception.ServerException;

public class HttpUtil {

	private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	public static void main(String[] args) {

	}
	
	/**
	 * http post 请求
	 * 
	 * @param url
	 * @param body
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static String httpPost(String url, String body) throws ClientProtocolException, IOException {
		String responseBody = "";

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);
		StringEntity myEntity = new StringEntity(body, ContentType.create("text/plain", "UTF-8"));
		post.setEntity(myEntity);
		
		logger.debug("Executing request " + post.getRequestLine()+"  \n  body----->>"+body);
		CloseableHttpResponse response = httpclient.execute(post);
		logger.debug("Executed response " +response.getStatusLine().toString());
		
		try {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				responseBody = EntityUtils.toString(entity, "utf-8");
			}
		} catch (Exception e) {
			logger.error("http post response Entity to String 失败",e);
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				logger.error("http post response 链接关闭失败",e);
			}
		}
		return responseBody;
	}

	public static String httpGet(String url) {
		String body = "";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

			HttpGet httpget = new HttpGet(url);

			logger.debug("Executing request " + httpget.getRequestLine());

			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				logger.debug(response.getStatusLine().toString());

				// Get hold of the response entity
				HttpEntity entity = response.getEntity();

				// If the response does not enclose an entity, there is no need
				// to bother about connection release
				if (entity != null) {
					body = EntityUtils.toString(entity, "utf-8");
					logger.debug(body);
				}

			} finally {
				response.close();
			}
		} catch (ClientProtocolException e) {
			logger.error("http get response Entity to String 失败",e);
		} catch (IOException e) {
			logger.error("http get response Entity to String 失败",e);
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				logger.error("http post response 链接关闭失败",e);
			}

		}

		return body;

	}
	
	/**
	 * 读取微信素图片资源
	 * @param url 
	 * @param body 
	 * @param imageFilePathname 需要保存的文件名（包含路径和名称，如路径不存在自动创建相应的目录）
	 * @param isScalr 是否缩放图片（120 x 120）
	 * @throws ServerException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws Exception 
	 */
	public static void httpPostForMedia(String url, String body,String imageFilePathname,boolean isScalr) throws ServerException, ClientProtocolException, IOException {

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);
		
		if(StringUtils.isNoneBlank(body)){
			StringEntity myEntity = new StringEntity(body, ContentType.create("text/plain", "UTF-8"));
			post.setEntity(myEntity);
		}
		
		logger.debug("Executing request " + post.getRequestLine()+"  \n  body----->>"+body);
		CloseableHttpResponse response = httpclient.execute(post);
		logger.debug("Executed response " +response.getStatusLine().toString());
		
		try {
			
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream responseBody = entity.getContent();
				
				File f=new File(imageFilePathname);
//				f.mkdirs();
				if(isScalr){
					//缩放图片
					BufferedImage thumbnail = Scalr.resize(ImageIO.read(responseBody), Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, 120, 120, Scalr.OP_ANTIALIAS);// 生成缩略图
					ImageIO.write(thumbnail, "png", f);
				}else{
					ImageIO.write(ImageIO.read(responseBody), "png", f);
				}
				logger.debug("创建图片："+imageFilePathname);
			}else{
				logger.debug("读取微信图片资源：没有任何图片资源，entity is null!");
			}
		} catch (Exception e) {
			logger.error("读取微信图片资源Error",e);
			throw new ServerException(Error._40001,Error._40001_MSG);
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
