/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.ly.wxstore.web;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ly.wxstore.comm.Error;
import com.ly.wxstore.entity.goods.StoreCart;
import com.ly.wxstore.exception.ApiErrorInfo;
import com.ly.wxstore.exception.ServerException;
import com.ly.wxstore.service.account.ShiroDbRealm.ShiroUser;
import com.ly.wxstore.service.goods.StoreCartService;

/**
 * 商城controller
 * 
 * @author peter
 */
@Controller
@RequestMapping(value = "/Cart")
public class CartController {


	private static Logger logger = LoggerFactory.getLogger(CartController.class);
	
	@Autowired
	private StoreCartService storeCartService;
	
	@RequiresRoles("user")
	@RequestMapping(value="goodsList.json",method = RequestMethod.GET)
	@ResponseBody
	public List<StoreCart> list() {
		try {
			List<StoreCart> list = storeCartService.getBySid(getCurrentUser().getSid());
			return list;
		} catch (ServerException e) {
			logger.error(e.getMessage(),e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}
	
	/**
	 * 添加到购物车
	 * @param goodsId
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value="addToCart/{goodsId}.json",method = RequestMethod.GET)
	@ResponseBody
	public void addToCart(@PathVariable("goodsId") Long goodsId) {
		logger.info("addToCart："+goodsId);
		try {
			storeCartService.addToCart(goodsId,getCurrentUser());
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}
	
	/**
	 * 删除购物车
	 * 
	 * @return
	 */
	@RequiresRoles("user")
	@RequestMapping(value = "delete/{cartId}.json", method = RequestMethod.DELETE)
	@ResponseBody
	public void delete(@PathVariable("cartId") Long cartId) {
		logger.info("Address/saveAddress");
		try {
			storeCartService.delete(cartId);
		} catch (ServerException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServerException(Error._10001, Error._10001_MSG);
		}
	}
	
	/**
	 * 使用Ehcache缓存 ShiroUse 信息，如果缓存失效，user里面的数据将会丢失
	 * 
	 * @return
	 */
	private ShiroUser getCurrentUser() {
		ShiroUser user = (ShiroUser) SecurityUtils.getSubject().getPrincipal();
		return user;
	}
	
	@ExceptionHandler(ServerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ApiErrorInfo handleInvalidRequestError(ServerException ex) {
		return new ApiErrorInfo(ex.getCode(), ex.getMessage());
	}

}
