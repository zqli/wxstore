package com.ly.wxstore.vo;

import com.ly.wxstore.comm.ResultCode;

/**
 * controller 层ajax 返回数据 结果对象
 * 
 * @author Peter
 *
 */
public class JsonResult {

	private boolean success = false;
	private String message = ResultCode.ERROR_MSG;
	private String code = ResultCode.ERROR;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
