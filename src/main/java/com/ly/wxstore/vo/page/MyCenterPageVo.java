package com.ly.wxstore.vo.page;

import com.ly.wxstore.comm.SalerRole;

/**
 * 与MyCenter 页面对应的VO
 * 
 * @author Administrator
 *
 */
public class MyCenterPageVo {

	public String nickname;// 昵称
	public String isSaler;// 是否是销售
	public String createDate;// 关注事件
	public String umcode;// 用户营销编号
	public String imageUrl;// 头像

	public String salerMoney = "0";// 销售额
	public String brokerage = "0";// 已结佣金
	public String brokeragePool = "0";// 待结佣金

	public Long downNumber;// 下线总人数
	public Long level_1_number;// 1级下线人数
	public Long level_2_number;// 2级下线人数
	public Long level_3_number;// 3级下线人数

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getIsSaler() {
		return isSaler;
	}

	public void setIsSaler(String salerRole) {
		if (SalerRole.fans.equals(salerRole)) {
			this.isSaler = "否";
		} else {
			this.isSaler = "是";
		}
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUmcode() {
		return umcode;
	}

	public void setUmcode(String umcode) {
		this.umcode = umcode;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getSalerMoney() {
		return salerMoney;
	}

	public void setSalerMoney(String salerMoney) {
		this.salerMoney = salerMoney;
	}

	public String getBrokerage() {
		return brokerage;
	}

	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}

	public String getBrokeragePool() {
		return brokeragePool;
	}

	public void setBrokeragePool(String brokeragePool) {
		this.brokeragePool = brokeragePool;
	}

	public Long getDownNumber() {
		return downNumber;
	}

	public void setDownNumber(Long downNumber) {
		this.downNumber = downNumber;
	}

	public Long getLevel_1_number() {
		return level_1_number;
	}

	public void setLevel_1_number(Long level_1_number) {
		this.level_1_number = level_1_number;
	}

	public Long getLevel_2_number() {
		return level_2_number;
	}

	public void setLevel_2_number(Long level_2_number) {
		this.level_2_number = level_2_number;
	}

	public Long getLevel_3_number() {
		return level_3_number;
	}

	public void setLevel_3_number(Long level_3_number) {
		this.level_3_number = level_3_number;
	}

}
